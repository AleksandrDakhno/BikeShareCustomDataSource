package com.aleksandrdakhno.bikeshare.datasource.stations;


import com.aleksandrdakhno.bikeshare.datasource.common.EntitiesDataSourceDefenitionService;
import com.aleksandrdakhno.bikeshare.services.metadata.MetadataService;
import com.aleksandrdakhno.bikeshare.services.metadata.entity.MetaDataField;
import net.sf.jasperreports.engine.JRDataSource;

import java.util.List;

/**
 * Created by aleksandrdakhno on 3/2/17.
 */

public class StationsDataSourceDefinitionService extends EntitiesDataSourceDefenitionService {

    protected List<MetaDataField> metadataFields() {
        return MetadataService.getInstance().getFields("stations");
    }

    protected JRDataSource dataSource(List<MetaDataField> fields) {
        return new StationsDataSource(fields);
    }

}
