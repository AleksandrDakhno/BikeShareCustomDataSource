package com.aleksandrdakhno.bikeshare.datasource.stations;

import com.aleksandrdakhno.bikeshare.datasource.common.EntitiesDataSource;
import com.aleksandrdakhno.bikeshare.iterators.DatasetIterator;
import com.aleksandrdakhno.bikeshare.iterators.StationsIterator;
import com.aleksandrdakhno.bikeshare.services.metadata.entity.MetaDataField;
import com.aleksandrdakhno.bikeshare.services.firebase.stations.entity.Station;

import java.util.List;

/**
 * Created by aleksandrdakhno on 3/2/17.
 */
class StationsDataSource extends EntitiesDataSource<Station> {
    private StationsIterator stationsIterator;

    StationsDataSource(List<MetaDataField> fields) {
        super(fields);
        stationsIterator = new StationsIterator();
    }

    protected DatasetIterator<Station> getEntitiesIterator() {
        return stationsIterator;
    }
}
