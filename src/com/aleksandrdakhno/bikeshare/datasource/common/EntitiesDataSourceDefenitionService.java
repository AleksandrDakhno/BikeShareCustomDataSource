package com.aleksandrdakhno.bikeshare.datasource.common;

import com.aleksandrdakhno.bikeshare.services.metadata.entity.MetaDataField;
import com.jaspersoft.jasperserver.api.metadata.jasperreports.domain.CustomDomainMetaData;
import com.jaspersoft.jasperserver.api.metadata.jasperreports.domain.CustomDomainMetaDataProvider;
import com.jaspersoft.jasperserver.api.metadata.jasperreports.domain.CustomReportDataSource;
import com.jaspersoft.jasperserver.api.metadata.jasperreports.service.ReportDataSourceService;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRParameter;

import java.util.List;
import java.util.Map;

/**
 * Created by aleksandrdakhno on 3/10/17.
 */
public abstract class EntitiesDataSourceDefenitionService implements ReportDataSourceService, CustomDomainMetaDataProvider {

    protected abstract List<MetaDataField> metadataFields();
    protected abstract JRDataSource dataSource(List<MetaDataField> fields);

    /*
     * CustomDomainMetaDataProvider interface
     */

    public CustomDomainMetaData getCustomDomainMetaData(CustomReportDataSource customReportDataSource) throws Exception {
        return new CustomDomainMetaDataImpl(metadataFields());
    }

    /*
     * ReportDataSourceService interface
     */

    public void setReportParameterValues(Map map) {
        map.put(JRParameter.REPORT_DATA_SOURCE, dataSource(metadataFields()));
    }

    public void closeConnection() {

    }
}
