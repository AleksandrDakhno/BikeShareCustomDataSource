package com.aleksandrdakhno.bikeshare.datasource.common;

import com.aleksandrdakhno.bikeshare.iterators.DatasetIterator;
import com.aleksandrdakhno.bikeshare.services.firebase.common.entity.ServiceEntity;
import com.aleksandrdakhno.bikeshare.services.metadata.entity.MetaDataField;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import java.util.List;

/**
 * Created by aleksandrdakhno on 3/10/17.
 */
public abstract class EntitiesDataSource<T extends ServiceEntity> implements JRDataSource {

    private List<MetaDataField> fields;
    protected abstract DatasetIterator<T> getEntitiesIterator();

    private int currentFieldPosition;
    private T currentEntity;

    public EntitiesDataSource(List<MetaDataField> fields) {
        this.fields = fields;
    }

    /*
     * JRDataSource interface
     */

    public boolean next() throws JRException {
        if (currentEntity == null || currentFieldPosition == fields.size()) {
            if (getEntitiesIterator().hasNext()) {
                currentFieldPosition = 0;
                currentEntity = getEntitiesIterator().next();
                return true;
            }
            return false;
        } else {
            return true; // iterating over fields for current station
        }
    }

    public Object getFieldValue(JRField jrField) throws JRException {
        currentFieldPosition++;
        return currentEntity.getFieldValue(jrField.getName()).getValue();
    }

}
