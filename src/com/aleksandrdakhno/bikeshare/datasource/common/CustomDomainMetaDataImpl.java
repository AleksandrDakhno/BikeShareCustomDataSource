package com.aleksandrdakhno.bikeshare.datasource.common;

import com.aleksandrdakhno.bikeshare.services.metadata.entity.MetaDataField;
import com.jaspersoft.jasperserver.api.metadata.jasperreports.domain.CustomDomainMetaData;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.design.JRDesignField;

import java.util.*;

/**
 * Created by aleksandrdakhno on 3/3/17.
 */
public class CustomDomainMetaDataImpl implements CustomDomainMetaData {

    private List<JRField> metadataFields = new ArrayList<JRField>();

    CustomDomainMetaDataImpl(List<MetaDataField> fields) {
        for (MetaDataField field : fields) {
            JRDesignField jrf = new JRDesignField();
            jrf.setName(field.getName());
            jrf.setValueClassName(field.getType());
            jrf.setDescription("description");
            metadataFields.add(jrf);
        }
    }

    /*
     *  CustomDomainMetaData interface
     */

    public List<JRField> getJRFieldList() {
        return metadataFields;
    }
    public String getQueryText() {
        return null;
    }
    public String getQueryLanguage() {
        return null;
    }
    public Map<String, String> getFieldMapping() {
        return null;
    }
}
