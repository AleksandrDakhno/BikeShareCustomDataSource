package com.aleksandrdakhno.bikeshare.datasource.statuses;

import com.aleksandrdakhno.bikeshare.datasource.common.EntitiesDataSourceDefenitionService;
import com.aleksandrdakhno.bikeshare.services.metadata.MetadataService;
import com.aleksandrdakhno.bikeshare.services.metadata.entity.MetaDataField;
import net.sf.jasperreports.engine.JRDataSource;

import java.util.List;

/**
 * Created by aleksandrdakhno on 3/10/17.
 */
public class StatusesDataSourceDefenitionService extends EntitiesDataSourceDefenitionService {

    protected List<MetaDataField> metadataFields() {
        return MetadataService.getInstance().getFields("statuses");
    }

    protected JRDataSource dataSource(List<MetaDataField> fields) {
        return new StatusesDataSource(fields);
    }

}
