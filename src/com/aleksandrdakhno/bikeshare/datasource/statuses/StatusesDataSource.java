package com.aleksandrdakhno.bikeshare.datasource.statuses;

import com.aleksandrdakhno.bikeshare.datasource.common.EntitiesDataSource;
import com.aleksandrdakhno.bikeshare.iterators.DatasetIterator;
import com.aleksandrdakhno.bikeshare.iterators.StatusesIterator;
import com.aleksandrdakhno.bikeshare.services.metadata.entity.MetaDataField;
import com.aleksandrdakhno.bikeshare.services.firebase.stations.entity.Status;

import java.util.List;

/**
 * Created by aleksandrdakhno on 3/10/17.
 */
class StatusesDataSource extends EntitiesDataSource<Status> {
    private StatusesIterator statusesIterator;

    StatusesDataSource(List<MetaDataField> fields) {
        super(fields);
        statusesIterator = new StatusesIterator();
    }

    protected DatasetIterator<Status> getEntitiesIterator() {
        return statusesIterator;
    }
}
