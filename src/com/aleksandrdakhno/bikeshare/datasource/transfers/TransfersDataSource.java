package com.aleksandrdakhno.bikeshare.datasource.transfers;

import com.aleksandrdakhno.bikeshare.datasource.common.EntitiesDataSource;
import com.aleksandrdakhno.bikeshare.iterators.DatasetIterator;
import com.aleksandrdakhno.bikeshare.iterators.TransfersIterator;
import com.aleksandrdakhno.bikeshare.services.firebase.transfers.entity.Transfer;
import com.aleksandrdakhno.bikeshare.services.metadata.entity.MetaDataField;

import java.util.List;

/**
 * Created by aleksandrdakhno on 3/10/17.
 */
public class TransfersDataSource extends EntitiesDataSource<Transfer> {
    private TransfersIterator transfersIterator;

    TransfersDataSource(List<MetaDataField> fields) {
        super(fields);
        transfersIterator = new TransfersIterator();
    }

    protected DatasetIterator<Transfer> getEntitiesIterator() {
        return transfersIterator;
    }
}
