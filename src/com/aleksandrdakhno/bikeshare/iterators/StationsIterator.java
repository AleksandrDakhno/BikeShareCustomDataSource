package com.aleksandrdakhno.bikeshare.iterators;

import com.aleksandrdakhno.bikeshare.services.StationsService;
import com.aleksandrdakhno.bikeshare.iterators.entity.Page;
import com.aleksandrdakhno.bikeshare.services.firebase.stations.entity.Station;

import java.util.List;

/**
 * Created by aleksandrdakhno on 3/7/17.
 */
public class StationsIterator extends EntitiesIterator<Station> {

    private StationsService stationsService;

    public StationsIterator() {
        super(0, 100);
        stationsService = new StationsService.Builder().build();
    }

    protected List<Station> fetchEntities() {
        return stationsService.fetchStations(
                new Page(getOffset(), getSize())
        );
    }
}
