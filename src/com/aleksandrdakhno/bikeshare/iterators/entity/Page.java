package com.aleksandrdakhno.bikeshare.iterators.entity;

/**
 * Created by aleksandrdakhno on 3/7/17.
 */
public class Page {
    private int offset;
    private int size;

    public Page(int offset, int size) {
        this.offset = offset;
        this.size = size;
    }

    public int getOffset() {
        return offset;
    }

    public int getSize() {
        return size;
    }
}
