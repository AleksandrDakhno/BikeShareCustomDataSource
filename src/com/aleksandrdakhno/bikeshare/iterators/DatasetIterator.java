package com.aleksandrdakhno.bikeshare.iterators;

/**
 * Created by aleksandrdakhno on 3/3/17.
 */
public interface DatasetIterator<T> {
    boolean hasNext();
    T next();
}
