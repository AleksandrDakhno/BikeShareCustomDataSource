package com.aleksandrdakhno.bikeshare.iterators;

import com.aleksandrdakhno.bikeshare.iterators.entity.Page;
import com.aleksandrdakhno.bikeshare.services.StationsService;
import com.aleksandrdakhno.bikeshare.services.firebase.stations.entity.Status;

import java.util.List;

/**
 * Created by aleksandrdakhno on 3/10/17.
 */
public class StatusesIterator extends EntitiesIterator<Status> {

    private StationsService stationsService;

    public StatusesIterator() {
        super(0, 100);
        stationsService = new StationsService.Builder().build();
    }

    protected List<Status> fetchEntities() {
        return stationsService.fetchStatuses(
                new Page(getOffset(), getSize())
        );
    }
}
