package com.aleksandrdakhno.bikeshare.iterators;

import com.aleksandrdakhno.bikeshare.iterators.entity.Page;
import com.aleksandrdakhno.bikeshare.services.TransfersService;
import com.aleksandrdakhno.bikeshare.services.firebase.transfers.entity.Transfer;

import java.util.List;

/**
 * Created by aleksandrdakhno on 3/10/17.
 */
public class TransfersIterator extends EntitiesIterator<Transfer>{

    private TransfersService transfersService;

    public TransfersIterator() {
        super(0, 100);
        transfersService = new TransfersService.Builder().build();
    }

    protected List<Transfer> fetchEntities() {
        return transfersService.fetchTransfers(
                new Page(getOffset(), getSize())
        );
    }
}
