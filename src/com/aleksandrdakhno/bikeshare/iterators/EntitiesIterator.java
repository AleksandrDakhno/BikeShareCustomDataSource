package com.aleksandrdakhno.bikeshare.iterators;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleksandrdakhno on 3/9/17.
 */
abstract class EntitiesIterator<T> implements DatasetIterator<T> {
    private List<T> entities;
    private int currentIndex;

    private int offset;
    private int size;

    EntitiesIterator(int offset, int size) {
        entities = new ArrayList<T>();
        this.offset = offset;
        this.size = size;
    }

    protected abstract List<T> fetchEntities();

    protected int getOffset() {
        return offset;
    }

    protected int getSize() {
        return size;
    }

    /*
     * DatasetIterator<T> interface
     */

    public boolean hasNext() {
        if (entities.size() == 0 || currentIndex == entities.size() ) {
            entities = fetchEntities();
            if (entities.size() > 0) {
                currentIndex = 0;
                offset += size;
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    public T next() {
        return entities.get(currentIndex++);
    }
}
