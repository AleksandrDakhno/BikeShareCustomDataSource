package com.aleksandrdakhno.bikeshare.services.metadata.entity;

/**
 * Created by aleksandrdakhno on 3/3/17.
 */
public class MetaDataField {
    private String name;
    private String type;

    public MetaDataField(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }
}
