package com.aleksandrdakhno.bikeshare.services.metadata;

import com.aleksandrdakhno.bikeshare.services.metadata.entity.MetaDataField;

import java.util.*;

/**
 * Created by aleksandrdakhno on 3/9/17.
 */
public class MetadataService {

    private List<MetaDataField> stationsFields = new ArrayList<MetaDataField>(Arrays.asList(new MetaDataField[]{
            new MetaDataField("id", "java.lang.Integer"),
            new MetaDataField("name", "java.lang.String"),
            new MetaDataField("city", "java.lang.String"),
            new MetaDataField("dock_count", "java.lang.Integer"),
            new MetaDataField("installation_date", "java.lang.String"),
            new MetaDataField("lat", "java.lang.Double"),
            new MetaDataField("long", "java.lang.Double")
    }));

    private List<MetaDataField> statusesFields = new ArrayList<MetaDataField>(Arrays.asList(new MetaDataField[]{
            new MetaDataField("station_id", "java.lang.Integer"),
            new MetaDataField("bikes_available", "java.lang.Integer"),
            new MetaDataField("docks_available", "java.lang.Integer")
    }));

    private List<MetaDataField> transfersFields = new ArrayList<MetaDataField>(Arrays.asList(new MetaDataField[]{
            new MetaDataField("id", "java.lang.Integer"),
            new MetaDataField("bikes_count", "java.lang.Integer"),
            new MetaDataField("date", "java.lang.String"),
            new MetaDataField("from_station_id", "java.lang.Integer"),
            new MetaDataField("to_station_id", "java.lang.Integer"),
    }));

    private Map<String, List<MetaDataField>> fields = new HashMap<String, List<MetaDataField>>();

    private MetadataService() {
        fields.put("stations", stationsFields);
        fields.put("statuses", statusesFields);
        fields.put("transfers", transfersFields);
    }

    private static MetadataService instance;
    public static MetadataService getInstance() {
        if (instance == null) {
            instance = new MetadataService();
        }
        return instance;
    }

    public List<MetaDataField> getFields(String tableName) {
        return fields.get(tableName);
    }
}
