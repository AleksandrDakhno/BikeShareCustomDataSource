package com.aleksandrdakhno.bikeshare.services;

import com.aleksandrdakhno.bikeshare.iterators.entity.Page;
import com.aleksandrdakhno.bikeshare.services.firebase.transfers.FirebaseTransfersService;
import com.aleksandrdakhno.bikeshare.services.firebase.transfers.entity.Transfer;

import java.util.List;

/**
 * Created by aleksandrdakhno on 3/10/17.
 */
public interface TransfersService {
    List<Transfer> fetchTransfers(Page page);

    class Builder {
        public TransfersService build() {
            return new FirebaseTransfersService();
        }
    }
}
