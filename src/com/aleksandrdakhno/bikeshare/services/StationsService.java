package com.aleksandrdakhno.bikeshare.services;

import com.aleksandrdakhno.bikeshare.iterators.entity.Page;
import com.aleksandrdakhno.bikeshare.services.firebase.stations.FirebaseStationsService;
import com.aleksandrdakhno.bikeshare.services.firebase.stations.entity.Station;
import com.aleksandrdakhno.bikeshare.services.firebase.stations.entity.Status;

import java.util.List;

/**
 * Created by aleksandrdakhno on 3/7/17.
 */
public interface StationsService {
    List<Station> fetchStations(Page page);
    List<Status> fetchStatuses(Page page);

    class Builder {
        public StationsService build() {
            return new FirebaseStationsService();
        }
    }
}
