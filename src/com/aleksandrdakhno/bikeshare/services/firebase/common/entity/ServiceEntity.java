package com.aleksandrdakhno.bikeshare.services.firebase.common.entity;

import java.util.List;

/**
 * Created by aleksandrdakhno on 3/10/17.
 */
public abstract class ServiceEntity {
    protected List<FieldValue> fieldValues;

    public FieldValue getFieldValue(String name) {
        for (FieldValue fieldValue : fieldValues) {
            if (fieldValue.getName().equals(name)) {
                return fieldValue;
            }
        }
        return null;
    }
}
