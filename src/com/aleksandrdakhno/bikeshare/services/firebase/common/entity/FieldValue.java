package com.aleksandrdakhno.bikeshare.services.firebase.common.entity;

/**
 * Created by aleksandrdakhno on 3/3/17.
 */
public class FieldValue {
    private String name;
    private Object value;

    public FieldValue(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Object getValue() {
        return value;
    }
}
