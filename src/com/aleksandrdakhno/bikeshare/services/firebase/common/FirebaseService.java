package com.aleksandrdakhno.bikeshare.services.firebase.common;

import com.aleksandrdakhno.bikeshare.driver.firebase.FirebaseDriver;
import com.aleksandrdakhno.bikeshare.iterators.entity.Page;
import com.aleksandrdakhno.bikeshare.services.firebase.common.mapper.EntitiesMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by aleksandrdakhno on 3/10/17.
 */
public class FirebaseService {

    private FirebaseDriver driver;

    protected FirebaseService() {
        driver = new FirebaseDriver();
    }

    protected<T> List<T> fetchEntities(String tableName, Page page, EntitiesMapper<T> mapper) {
        try {
            String responseString = driver.makeRequest(
                    tableName,
                    page.getOffset(),
                    page.getSize()
            );

            ObjectMapper jsonMapper = new ObjectMapper();
            JsonNode node = jsonMapper.readTree(responseString);

            List<T> entities = new ArrayList<T>();
            if (node instanceof ArrayNode) {
                ArrayNode arrayNode = (ArrayNode) node;
                Iterator<JsonNode> iterator = arrayNode.elements();
                int i = 0;
                while (iterator.hasNext()) {
                    JsonNode childNode = iterator.next();
                    if (childNode.isNull()) {
                        i++;
                        continue;
                    }
                    T status = mapper.mapEntity(i, childNode);
                    entities.add(status);
                    i++;
                }
            } else if(node instanceof ObjectNode) {
                ObjectNode objectNode = (ObjectNode) node;
                Iterator<Map.Entry<String, JsonNode>> iterator = objectNode.fields();
                while (iterator.hasNext()) {
                    Map.Entry<String, JsonNode> entry = iterator.next();
                    String key = entry.getKey();
                    JsonNode childNode = entry.getValue();
                    T status = mapper.mapEntity(Integer.parseInt(key), childNode);
                    entities.add(status);
                }
            }
            return entities;
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<T>();
        }
    }

}
