package com.aleksandrdakhno.bikeshare.services.firebase.common.mapper;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Created by aleksandrdakhno on 3/10/17.
 */
public interface EntitiesMapper<T> {
    public T mapEntity(int id, JsonNode node);
}
