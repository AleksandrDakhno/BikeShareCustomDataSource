package com.aleksandrdakhno.bikeshare.services.firebase.transfers.entity;

import com.aleksandrdakhno.bikeshare.services.firebase.common.entity.FieldValue;
import com.aleksandrdakhno.bikeshare.services.firebase.common.entity.ServiceEntity;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by aleksandrdakhno on 3/10/17.
 */
public class Transfer extends ServiceEntity {
    public Transfer(int id, int bikesCount, String date, int fromStationId, int toStationId) {
        fieldValues = new ArrayList<FieldValue>(Arrays.asList(new FieldValue[]{
                new FieldValue("id", id),
                new FieldValue("bikes_count", bikesCount),
                new FieldValue("date", date),
                new FieldValue("from_station_id", fromStationId),
                new FieldValue("to_station_id", toStationId)
        }));
    }
}
