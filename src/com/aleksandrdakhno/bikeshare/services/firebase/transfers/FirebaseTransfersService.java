package com.aleksandrdakhno.bikeshare.services.firebase.transfers;

import com.aleksandrdakhno.bikeshare.iterators.entity.Page;
import com.aleksandrdakhno.bikeshare.services.TransfersService;
import com.aleksandrdakhno.bikeshare.services.firebase.common.FirebaseService;
import com.aleksandrdakhno.bikeshare.services.firebase.common.mapper.EntitiesMapper;
import com.aleksandrdakhno.bikeshare.services.firebase.transfers.entity.Transfer;
import com.aleksandrdakhno.bikeshare.services.firebase.transfers.mapper.TransfersMapper;

import java.util.List;

/**
 * Created by aleksandrdakhno on 3/10/17.
 */
public class FirebaseTransfersService extends FirebaseService implements TransfersService {

    private EntitiesMapper<Transfer> transferMapper;

    public FirebaseTransfersService() {
        transferMapper = new TransfersMapper();
    }

    /*
     * TransfersService interface
     */

    public List<Transfer> fetchTransfers(Page page) {
        return fetchEntities("transfers", page, transferMapper);
    }
}
