package com.aleksandrdakhno.bikeshare.services.firebase.transfers.mapper;

import com.aleksandrdakhno.bikeshare.services.firebase.common.mapper.EntitiesMapper;
import com.aleksandrdakhno.bikeshare.services.firebase.transfers.entity.Transfer;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * Created by aleksandrdakhno on 3/10/17.
 */
public class TransfersMapper implements EntitiesMapper<Transfer> {
    public Transfer mapEntity(int id, JsonNode node) {
        return new Transfer(
                id,
                node.path("bikes_count").asInt(),
                node.path("date").asText(),
                node.path("from_station_id").asInt(),
                node.path("to_station_id").asInt()
        );
    }
}
