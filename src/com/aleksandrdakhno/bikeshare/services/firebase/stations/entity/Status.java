package com.aleksandrdakhno.bikeshare.services.firebase.stations.entity;

import com.aleksandrdakhno.bikeshare.services.firebase.common.entity.FieldValue;
import com.aleksandrdakhno.bikeshare.services.firebase.common.entity.ServiceEntity;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by aleksandrdakhno on 3/10/17.
 */
public class Status extends ServiceEntity {
    public Status(int stationId, int bikesAvailable, int docksAvailable) {
        fieldValues = new ArrayList<FieldValue>(Arrays.asList(new FieldValue[]{
                new FieldValue("station_id", stationId),
                new FieldValue("bikes_available", bikesAvailable),
                new FieldValue("docks_available", docksAvailable)
        }));
    }
}
