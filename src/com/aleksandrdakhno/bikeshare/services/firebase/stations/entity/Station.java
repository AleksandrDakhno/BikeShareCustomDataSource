package com.aleksandrdakhno.bikeshare.services.firebase.stations.entity;

import com.aleksandrdakhno.bikeshare.services.firebase.common.entity.FieldValue;
import com.aleksandrdakhno.bikeshare.services.firebase.common.entity.ServiceEntity;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by aleksandrdakhno on 3/3/17.
 */
public class Station extends ServiceEntity {
    public Station(int id, String name, String city, String installationDate, int dockCount, Double latitude, Double longitude) {
        fieldValues = new ArrayList<FieldValue>(Arrays.asList(new FieldValue[]{
                new FieldValue("id", id),
                new FieldValue("name", name),
                new FieldValue("city", city),
                new FieldValue("dock_count", dockCount),
                new FieldValue("installation_date", installationDate),
                new FieldValue("lat", latitude),
                new FieldValue("long", longitude),
        }));
    }
}
