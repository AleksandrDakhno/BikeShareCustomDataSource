package com.aleksandrdakhno.bikeshare.services.firebase.stations;

import com.aleksandrdakhno.bikeshare.iterators.entity.Page;
import com.aleksandrdakhno.bikeshare.services.StationsService;
import com.aleksandrdakhno.bikeshare.services.firebase.common.FirebaseService;
import com.aleksandrdakhno.bikeshare.services.firebase.stations.entity.Station;
import com.aleksandrdakhno.bikeshare.services.firebase.stations.entity.Status;
import com.aleksandrdakhno.bikeshare.services.firebase.common.mapper.EntitiesMapper;
import com.aleksandrdakhno.bikeshare.services.firebase.stations.mapper.StationsMapper;
import com.aleksandrdakhno.bikeshare.services.firebase.stations.mapper.StatusesMapper;

import java.util.*;

/**
 * Created by aleksandrdakhno on 3/9/17.
 */
public class FirebaseStationsService extends FirebaseService implements StationsService {

    private EntitiesMapper<Station> stationsMapper;
    private EntitiesMapper<Status> statusesMapper;

    public static void main(String[] args) {
        // Tests
        FirebaseStationsService service = new FirebaseStationsService();
        List<Station> stations = service.fetchStations(new Page(0, 100));
        System.out.println("stations: " + stations.toString());
    }

    public FirebaseStationsService() {
        stationsMapper = new StationsMapper();
        statusesMapper = new StatusesMapper();
    }

    /*
     * StationsService interface
     */

    public List<Station> fetchStations(Page page) {
        return fetchEntities("stations", page, stationsMapper);
    }
    public List<Status> fetchStatuses(Page page) {
        return fetchEntities("stations_status", page, statusesMapper);
    }
}
