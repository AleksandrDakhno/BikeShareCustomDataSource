package com.aleksandrdakhno.bikeshare.services.firebase.stations;

import com.aleksandrdakhno.bikeshare.iterators.entity.Page;
import com.aleksandrdakhno.bikeshare.services.StationsService;
import com.aleksandrdakhno.bikeshare.services.firebase.stations.entity.Station;
import com.aleksandrdakhno.bikeshare.services.firebase.stations.entity.Status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by aleksandrdakhno on 3/9/17.
 */
public class FakeStationsService implements StationsService {

    private List<Station> stations = new ArrayList<Station>(Arrays.asList(new Station[]{
            new Station(
                    0,
                    "San Jose Diridon Caltrain Station",
                    "San Jose",
                    "2013-08-06",
                    27,
                    37.329732,
                    -121.90178200000001
            ),
            new Station(
                    1,
                    "San Jose Civic Center",
                    "San Jose",
                    "2013-08-05",
                    15,
                    37.330698,
                    -121.888979
            ),
            new Station(
                    2,
                    "San Jose Civic Center",
                    "San Jose",
                    "2013-08-05",
                    16,
                    37.330698,
                    -121.888979
            ),
            new Station(
                    3,
                    "San Jose Civic Center",
                    "San Jose",
                    "2013-08-05",
                    11,
                    37.330698,
                    -121.888979
            )
    }));

    private List<Status> statuses = new ArrayList<Status>(Arrays.asList(new Status[]{
            new Status(
                    0,
                    1,
                    2
            ),
            new Status(
                    1,
                    2,
                    3
            ),
            new Status(
                    3,
                    3,
                    4
            ),
            new Status(
                    4,
                    5,
                    6
            ),
    }));

    /*
     * StationsService interface
     */

    public List<Station> fetchStations(Page page) {
        int fromIndex = fromIndex(page);
        int toIndex = toIndex(page);

        if (fromIndex < toIndex) {
            return stations.subList(fromIndex, toIndex);
        } else {
            return new ArrayList<Station>();
        }
    }

    public List<Status> fetchStatuses(Page page) {
        int fromIndex = fromIndex(page);
        int toIndex = toIndex(page);

        if (fromIndex < toIndex) {
            return statuses.subList(fromIndex, toIndex);
        } else {
            return new ArrayList<Status>();
        }
    }

    /*
     * Private methods
     */

    private int fromIndex(Page page) {
        return page.getOffset();
    }
    private int toIndex(Page page) {
        int toIndex = page.getOffset() + page.getSize();
        if (toIndex > stations.size()) {
            toIndex = stations.size();
        }
        return toIndex;
    }
}
