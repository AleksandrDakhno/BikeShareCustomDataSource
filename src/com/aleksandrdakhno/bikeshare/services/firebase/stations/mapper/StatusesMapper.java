package com.aleksandrdakhno.bikeshare.services.firebase.stations.mapper;

import com.aleksandrdakhno.bikeshare.services.firebase.common.mapper.EntitiesMapper;
import com.aleksandrdakhno.bikeshare.services.firebase.stations.entity.Status;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * Created by aleksandrdakhno on 3/10/17.
 */
public class StatusesMapper implements EntitiesMapper<Status> {
    public Status mapEntity(int id, JsonNode node) {
        return new Status(
                id,
                node.path("bikes_available").asInt(),
                node.path("docks_available").asInt()
        );
    }
}
