package com.aleksandrdakhno.bikeshare.services.firebase.stations.mapper;

import com.aleksandrdakhno.bikeshare.services.firebase.common.mapper.EntitiesMapper;
import com.aleksandrdakhno.bikeshare.services.firebase.stations.entity.Station;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * Created by aleksandrdakhno on 3/10/17.
 */
public class StationsMapper implements EntitiesMapper<Station> {
    public Station mapEntity(int id, JsonNode node) {
        return new Station(
                id,
                node.path("name").asText(),
                node.path("city").asText(),
                node.path("installation_date").asText(),
                node.path("dock_count").asInt(),
                node.path("lat").asDouble(),
                node.path("long").asDouble()
        );
    }
}
