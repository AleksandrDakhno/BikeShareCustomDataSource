package com.aleksandrdakhno.bikeshare.driver.firebase;

import com.aleksandrdakhno.bikeshare.driver.network.NetworkClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by aleksandrdakhno on 3/2/17.
 */
public class FirebaseDriver {
    private final String baseURLString = "https://bike-share-65de6.firebaseio.com/";
    private NetworkClient networkClient = new NetworkClient();

    public String makeRequest(String tableName, int startAt, int limit) throws IOException {
        return networkClient.makeRequest(
                "GET",
                baseURLString + tableName.toLowerCase() + ".json?orderBy=\"$key\"&startAt=\"" + startAt + "\"&limitToFirst="+ limit + ""
        );
    }
}
